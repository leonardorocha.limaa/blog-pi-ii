import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const NotFoundPage = () => (
  <Layout>
    <h1>NOT FOUND</h1>
    <p>Você acabou de pegar uma rota que não existe... que pena.</p>
  </Layout>
)

export default NotFoundPage
